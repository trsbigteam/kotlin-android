package workshop.gdg.kotlinandroid.data.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import workshop.gdg.kotlinandroid.data.database.dao.ProductDataDao
import workshop.gdg.kotlinandroid.data.database.model.ProductEntity

@Database(entities = arrayOf(ProductEntity::class), version = 1)
abstract class ProductDataBase : RoomDatabase() {

    abstract fun productDataDao(): ProductDataDao

    companion object {
        private var INSTANCE: ProductDataBase? = null

        fun getInstance(context: Context): ProductDataBase {
            if (INSTANCE == null) {
                synchronized(ProductDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ProductDataBase::class.java, "product.db")
                            .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}