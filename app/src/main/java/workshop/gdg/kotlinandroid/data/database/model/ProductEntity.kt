package workshop.gdg.kotlinandroid.data.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "productEntity")
data class ProductEntity(
    @PrimaryKey(autoGenerate = false) var id: Long,
    val imageIngredientsThumbUrl: String? = null,
    @ColumnInfo(name = "nutrition_grade_fr")
    val nutritionGradeFr: String? = null,
    @ColumnInfo(name = "allergens")
    val name: String,
    @ColumnInfo(name = "product_name")
    val allergens: String? = null,
    @ColumnInfo(name = "image_url")
    val imageUrl: String? = null,
    @ColumnInfo(name = "brands")
    val brands: String? = null
)