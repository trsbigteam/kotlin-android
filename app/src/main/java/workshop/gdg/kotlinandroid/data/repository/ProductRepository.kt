package workshop.gdg.kotlinandroid.data.repository

import workshop.gdg.kotlinandroid.presentation.model.Product

interface ProductRepository{

    suspend fun getProduct(id : String) : Product?
    suspend fun getAllProduct() : List<Product>
}