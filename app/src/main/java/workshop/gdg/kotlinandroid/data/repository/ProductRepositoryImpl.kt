package workshop.gdg.kotlinandroid.data.repository

import android.app.Application
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import workshop.gdg.kotlinandroid.data.database.ProductDataBase
import workshop.gdg.kotlinandroid.data.database.dao.ProductDataDao
import workshop.gdg.kotlinandroid.data.web.model.GetProductResponse
import workshop.gdg.kotlinandroid.presentation.model.Product
import workshop.gdg.kotlinandroid.presentation.model.toPresentation
import java.net.URL

class ProductRepositoryImpl(val application: Application, val dispatcher: CoroutineDispatcher = Dispatchers.IO) : ProductRepository {

    private val productDataDao: ProductDataDao by lazy {
        ProductDataBase.getInstance(application).productDataDao()
    }

    override suspend fun getProduct(id: String): Product? =
        withContext(dispatcher) {
            retrieveProductFromWebAndCache(id)
        }

    override suspend fun getAllProduct(): List<Product> =
        withContext(dispatcher) {
            productDataDao.getAll().map { it.toPresentation() }
        }


    private fun retrieveProductFromWebAndCache(id: String): Product? {
        val result = URL("https://fr.openfoodfacts.org/api/v0/produit/$id.json").readText()
        val productResponse = Gson().fromJson(result, GetProductResponse::class.java)
        val productEntity = productResponse.toEntity()
        productEntity?.let { productDataDao.insert(it) }
        return productEntity?.toPresentation()
    }
}