package workshop.gdg.kotlinandroid.data.web.model

import com.google.gson.annotations.SerializedName

class ProductResponse {

    @SerializedName("image_ingredients_thumb_url")
    var imageIngredientsThumbUrl: String? = null
    @SerializedName("nutrition_grade_fr")
    var nutritionGradeFr: String? = null
    @SerializedName("allergens")
    var allergens: String? = null
    @SerializedName("image_url")
    var imageUrl: String? = null
    @SerializedName("product_name")
    var name: String = ""
    @SerializedName("brands")
    var brands: String = ""
}
