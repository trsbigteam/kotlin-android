package workshop.gdg.kotlinandroid.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import workshop.gdg.kotlinandroid.presentation.scope.ActivityScope

abstract class BaseActivity : AppCompatActivity() {
    protected val uiScope = ActivityScope()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(uiScope)
    }
}