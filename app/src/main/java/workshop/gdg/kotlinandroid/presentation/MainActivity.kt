package workshop.gdg.kotlinandroid.presentation

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.data.repository.ProductRepository
import workshop.gdg.kotlinandroid.data.repository.ProductRepositoryImpl
import workshop.gdg.kotlinandroid.extensions.launchActivity
import workshop.gdg.kotlinandroid.presentation.model.Product

class MainActivity : BaseActivity(), OnItemClickedListener {

    private val repository: ProductRepository by lazy { ProductRepositoryImpl(application) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeRecyclerView()
        setButtonClickAction()
    }

    private fun initializeRecyclerView() {
        with(productListRecyclerView) {
            adapter = ProductListAdapter(arrayListOf(), this@MainActivity, this@MainActivity)
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@MainActivity)
            addItemDecoration(
                androidx.recyclerview.widget.DividerItemDecoration(
                    this@MainActivity,
                    androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
                )
            )
        }
    }

    private fun setButtonClickAction() {
        goToScannerButton.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 0)
            } else {
                launchActivity<ScanActivity>()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        uiScope.launch {
            val products = repository.getAllProduct()
            updateUI(products)
        }
    }

    override fun onItemClicked(product: Product) {
        launchActivity<ProductDetailsActivity>(ProductDetailsActivity.EXTRA_PRODUCT_ID to product.code.toString())
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            launchActivity<ScanActivity>()
        } else {
            Toast.makeText(this, "Accepter la permission afin d'utiliser l'application", Toast.LENGTH_LONG).show()
        }
        return
    }

    private fun updateUI(productResponse: List<Product>) {
        productListRecyclerView.adapter = ProductListAdapter(ArrayList(productResponse), this, this)
    }
}
