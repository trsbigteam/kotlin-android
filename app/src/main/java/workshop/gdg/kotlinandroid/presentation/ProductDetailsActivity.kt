package workshop.gdg.kotlinandroid.presentation

import android.os.Bundle
import kotlinx.android.synthetic.main.product_details_activity.*
import kotlinx.coroutines.launch
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.data.repository.ProductRepository
import workshop.gdg.kotlinandroid.data.repository.ProductRepositoryImpl
import workshop.gdg.kotlinandroid.presentation.model.Product
import workshop.gdg.kotlinandroid.presentation.model.getGrade
import workshop.gdg.kotlinandroid.presentation.model.getGradeColor
import workshop.gdg.kotlinandroid.presentation.model.loadProductImage

class ProductDetailsActivity : BaseActivity() {

    val context = this@ProductDetailsActivity

    companion object {
        const val EXTRA_PRODUCT_ID = "EXTRA_PRODUCT_ID"
    }

    private val repository : ProductRepository by lazy { ProductRepositoryImpl(application) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_details_activity)
        val productId = intent?.extras?.get(EXTRA_PRODUCT_ID) as String
        getProduct(productId)
    }

    private fun getProduct(id: String) {
        uiScope.launch {
            val product = repository.getProduct(id = id)
            updateUI(product)
        }
    }

    private fun updateUI(product: Product?) {
        product?.let {
            with(it) {
                product.loadProductImage(productDetailsImage)
                productDetailsName.text = name
                productDetailsBrand.text = brands
                productDetailsBarCode.text = code.toString()
                productDetailsGrade.text = context.getString(product.getGrade(context))
                productDetailsGradeColor.setBackgroundColor(product.getGradeColor(context))
            }
        }
    }
}