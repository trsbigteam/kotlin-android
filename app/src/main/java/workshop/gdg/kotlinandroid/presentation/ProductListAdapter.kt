package workshop.gdg.kotlinandroid.presentation

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.product_list_item.view.*
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.presentation.model.Product
import workshop.gdg.kotlinandroid.presentation.model.getGrade
import workshop.gdg.kotlinandroid.presentation.model.getGradeColor
import workshop.gdg.kotlinandroid.presentation.model.loadProductImage

class ProductListAdapter(private val items: ArrayList<Product>, private val context: Context, private val listener: OnItemClickedListener) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.product_list_item, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = items[position]
        product.loadProductImage(holder.productImage)
        holder.productName.text = product.name
        holder.productBrands.text = product.brands
        holder.productBarCode.text = product.code.toString()
        holder.productGrade.text = holder.view.context.getString(product.getGrade(context))
        holder.productGraColor.setBackgroundColor(product.getGradeColor(context))
        holder.bind(product, listener)
    }
}


class ViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    val productImage: ImageView = view.productImage
    val productName: TextView = view.productName
    val productBrands: TextView = view.productBrand
    val productGrade: TextView = view.productGrade
    val productGraColor: ImageView = view.productGradeColor
    val productBarCode: TextView = view.productBarCode

    fun bind(product: Product, listener: OnItemClickedListener) {
        view.setOnClickListener { listener.onItemClicked(product) }
    }
}

interface OnItemClickedListener {
    fun onItemClicked(product: Product)
}