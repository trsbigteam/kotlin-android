package workshop.gdg.kotlinandroid.presentation

import android.os.Bundle
import kotlinx.android.synthetic.main.scan_activity.*
import me.dm7.barcodescanner.zbar.Result
import me.dm7.barcodescanner.zbar.ZBarScannerView
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.extensions.launchActivity
import workshop.gdg.kotlinandroid.presentation.ProductDetailsActivity.Companion.EXTRA_PRODUCT_ID


class ScanActivity : BaseActivity(), ZBarScannerView.ResultHandler {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scan_activity)
    }

    override fun onResume() {
        super.onResume()
        scannerView.setResultHandler(this)
        scannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        scannerView.stopCamera()
    }

    override fun handleResult(result: Result?) {
        result?.let {
            launchActivity<ProductDetailsActivity>(EXTRA_PRODUCT_ID to it.contents)
            finish()
        }
    }
}

