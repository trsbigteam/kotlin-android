package workshop.gdg.kotlinandroid.presentation.model

import android.content.Context
import androidx.core.content.ContextCompat
import android.widget.ImageView
import workshop.gdg.kotlinandroid.R
import workshop.gdg.kotlinandroid.data.database.model.ProductEntity
import workshop.gdg.kotlinandroid.presentation.GlideApp

data class Product constructor(
    val code: Long,
    val name: String,
    var imageIngredientsThumbUrl: String? = null,
    var nutritionGradeFr: String? = null,
    var allergens: String? = null,
    var imageUrl: String? = null,
    var brands: String? = null
)


fun ProductEntity.toPresentation() =
    Product(id, name, imageIngredientsThumbUrl, nutritionGradeFr, allergens, imageUrl, brands)

fun Product.getGradeColor(context: Context) = with(nutritionGradeFr) {
    if (nutritionGradeFr != null) {
        when (this?.toLowerCase()) {
            "a" -> ContextCompat.getColor(context, R.color.a)
            "b" -> ContextCompat.getColor(context, R.color.b)
            "c" -> ContextCompat.getColor(context, R.color.c)
            "d" -> ContextCompat.getColor(context, R.color.d)
            "e" -> ContextCompat.getColor(context, R.color.e)
            else -> ContextCompat.getColor(context, R.color.not_available)
        }
    } else {
        ContextCompat.getColor(context, R.color.not_available)
    }
}

fun Product.getGrade(context: Context) = with(nutritionGradeFr) {
    if (nutritionGradeFr != null) {
        when (this?.toLowerCase()) {
            "a" -> R.string.a
            "b" -> R.string.b
            "c" -> R.string.c
            "d" -> R.string.d
            "e" -> R.string.e
            else -> R.string.not_available
        }
    } else {
        R.string.not_available
    }
}

fun Product.loadProductImage(view: ImageView) =
    GlideApp.with(view).load(imageUrl ?: R.drawable.not_available).into(view)
